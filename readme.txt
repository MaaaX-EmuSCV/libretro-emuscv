===========================================================
Libretro-EmuSCV (EPOCH/YENO Super Cassette Vision Emulator)
===========================================================

-------
 About
-------
Libretro-EmuSCV emulte the "EPOCH/YENO Super Cassette Vision", a home video
game console made by EPOCH CO. and released in Japan on July 17, 1984 and
released in Europe (France only) later in 1984 under the YENO brand.

This emulator is purposed as a crossplatform core for Libretro compatible
fondends as Retroarch, Recalbox, RetroPie, etc.
It is written by MARCONATO Maxime (aka MaaaX^^, aka EPOCH84) and is based
on eSCV, an "EPOCH Super Cassette Vision Emulator for Win32" written by
TAKEDA toshiya.



-----------------
 For developpers
-----------------
Please use Visual Studio Code as editor on Windows, Mac and Linux, 
On Windows, install Mingw64 as described on the Libretro dev page.
On Linux install build-essential and retroarch (not from flatpak).
On Mac please install "xcode-select --install" (debug with lldb on OS X 10.9+).



---------
 Authors
---------

Libretro-EmuSCV:
- MARCONATO Maxime (aka MaaaX^^, aka EPOCH84) | maxime@maaax.com | http://www.maaax.com/

eSCV:
- TAKEDA toshiya | takeda@m1.interq.or.jp | http://takeda-toshiya.my.coocan.jp/



----------
 Binaries
----------

All binaries:
https://www.maaax.com/emuscv/binaries/

Last version:
https://www.maaax.com/emuscv/binaries/last/



--------------
 Installation
--------------

Use core depending of your OS:
- Windows       : "emuscv_libretro.dll"
- Mac OS / OS X : "emuscv_libretro.dylib"
- Linux         : "emuscv_libretro.so"
Place the EmuSCV core in the core directory of the front end

For Retroarch, place the "emuscv_libretro.info" file in the info directory too.



------
 BIOS
------

EmuSCV require a Super Cassette Vision BIOS to run.

There is only one version of the BIOS that can use diferent names:
- upd7801g.s01 (standard)
- upd7801g.bin
- upd7801g.bios
- bios.rom
- bios.bin

MD5: 635a978fd40db9a18ee44eff449fc126

The MD5 Checksum control for the BIOS can be disabled in core options to permit use of custom BIOS.



-------------
 Game / ROMs
-------------

Supported ROMs extensions: .CART (native), .BIN, .ROM, .0 (.1, .2, .3)
and Zipped Roms (the name of the rom must be the same as the ZIP archive).

All ROMs can be stored in a 1 file ROM (.CART, .BIN, .ROM or .0).
Large ROMs can be stored in multiple files (.0, .1, .2, .3).

For technical reasons, except for the .CART format, only known games are supported (based on their MD5 checksum):

- ASTRO WARS
     (1 file of 8KB) MD5: 5c0a8b9e0ae3bdc62cf758c8de3c621c

- BASIC NYUMON
     (1 file of 32KB) MD5: 6da4e79f9dd60abe222ae8f95023ad48

- BATTLE IN GALAXY (ASTRO WARS II)
     (1 file of 8KB) MD5: 28b41f62cefae2524f6ae1b980d054d7

- BOULDER DASH
     (1 file of 16KB) MD5: 4471a0938102c21a49635d3e4efb61bc OR 21b1f2432b4ea67dc7162630100b2cd5

- COMIC CIRCUS
     (1 file of 16KB) MD5: ec261a6208094fceb7a4f8cc60db24b4

- DORAEMON
     (1 file of 64KB) MD5: e93f06b2ccbafa753a2eac14f92070c2
     (2 files of 32KB) MD5: 8928d34618d98384119ffa351d0293cf AND 3ac9c39b019a81fabb1e26f75292827c

- DRAGON BALL
     (1 file of 128KB) MD5: 6d11d49390d0946501b39d9f9688ae9d
     (4 files of 32KB) MD5: 61a6f0c4ef26db9d073b71b21ef957ea, 61a6f0c4ef26db9d073b71b21ef957ea, 122398143f8e538b26b0bd1356431022 AND 087179b0a06f30fe5b35a82dc45a6df9

- DRAGON SLAYER
     (1 file of 32KB) MD5: 9c688c8f43b142a84b90181d717df6d2

- ELEVATOR FIGHT
     (1 file of 16KB) 3ac6b89ba13e57100d522921abb7319c

- KUNG-FU ROAD
     (1 file of 40KB) MD5: fa05cb7422744b0d8a73125db576708f
     (2 files of 32KB and 8KB) MD5: 364c7dae2b5ee1d0525906c501b276a5 AND 4b2f0e32943ed64561a03e267d6f3fd9

- LUPIN III
     (1 file of 16KB) MD5: e1547118cf5a9e88825408f12550b890 OR 07e8dc7cbaacedbbcb483a4e7e6b18f3

- MAPPY
     (1 file of 32KB) MD5: 801619e946eb962fe13f4582751c7588

- MILKY PRINCESS
     (1 file of 32KB) MD5: 9449bddc1056d51c7147dc2e6329d2ed

- MINER 2049ER
     (1 file of 16KB) MD5: f4a3f4f5a08a15ec62a32e959a528eac

- NEBULA
     (1 file of 16KB) MD5: e86aab083fc9722f8a44b356139522c2 OR ac629947980bbd14478c29c1645efa41

- POLE POSITION II
     (1 file of 128KB) MD6: 78249cfb10962347a8001ba102992503
     (4 files of 32KB, first filled with 0xFF) MD5: 3df7b33399422731e8e5615785c0536d, 4a6d816aa720a8d921b0e38116d205e7, b1c2355e6c5f5eb48ee0d446223149e1 AND 7c3cb94c676502b8b149afdbf6fbd3f0

- POP & CHIPS
     (1 file of 32KB) MD5: e2488c33d92ef9985e6b62b45644303c

- PROFESSIONAL WRESTLING
     (1 file of 128KB) MD5: 44d432cb0093a1992beb806a68e7aaad 
     (4 files of 32KB) MD5: f5020cfd5fa59a06be927540638c5444, 8d1269381114d330757f91fb07e2d76d, f8328107cb60e3341e9c25c14af239a4 AND a3556d2c9ec743085590645b9bbbe885

- PUNCH BOY
     (1 file of 16KB) MD5: 7c10f8df512ce0bbfad7505ccca88827

- REAL MAHJONG
     (1 file of 16KB) MD5: c9e1042402b5a0ff6f75b737fec8a08a

- SHOGI
     (1 file of 32KB) MD5: b248fff8526d5c3cfba82a6e036815ca

- SKY KID
     (1 file of 64KB) MD5: 9b6dd35dd278bcfaa98d4ecf531654cf
     (2 files of 32KB) MD5: 9fa449f97ed26b328793a2ae7c3a7d51 AND a6f81f3b8351771ed7ba3b2f7e987d65

- STAR SPEEDER
     (1 file of 40KB) MD5: 3a4e3634b4390241beda0a94fa69564d
     (2 files of 32KB and 8KB) MD5: 3060d95c563115bde239375518ee6b1d AND 19c339d88cef61bde7048409828a42e7

- SUPER BASEBALL
     (1 file of 16KB) MD5: c800d70c4a1f9600407d8392b9455015

- SUPER GOLF
     (1 file of 16KB) MD5: 734091f00d410fc4251f0aae7d47e4c1

- SUPER SANSUPYUTA
     (1 file of 32KB) MD5: 05f2b3b1a5e450c8ead224c3ebf5b75f

- SUPER SOCCER
     (1 file of 16KB) MD5: 5cd60ad0a9bfb818db9c4e5accc05537 OR dd60e91b361fdc8bc8ead3d76c45897c

- TON TON BALL
     (1 file of 16KB) MD5: 622cbb0451fbf82b9b7834c5ca589443

- WHEELY RACER
     (1 file of 16KB) ef4ab0300b9e056d1ba9873b63a1b6cf

- Y2 MONSTER LAND
     (1 file of 128KB) MD5: 64d6268aebabedb1552c2cd565ec32a7
     (4 files of 32KB) MD5: c6baa36261ff82e8c454e59714ab4771, bfe6c003f5652ebe344139e3c2ed8052, 533e823555bc42d1c0547068a03f845e AND 465f41fc3d3b3a689856b108dbf229a7
     

The .CART file format is the native format for EmuSCV.
It includes the ROM and describes the ROM/RAM/SRAM bank wiring of the cart.
When a known ROM in format .bin, .rom, .0 (.1, .2, .3) is loaded, it is automatically converted in format .CART in save directory.
Some original game carts included batteries to save some data like score, custom levels, etc.
For these games EmuSCV create a .SAVE file in the save directory to save these data (and load data from it when launching the game).
These games are: BASIC NYUMON, DRAGON SLAYER and POP & CHIPS.



----------------------------
 EmuSCV native file formats
----------------------------

.CART file format V1
--------------------

The .CART format is the native ROM format for EmuSCV.
It includes the ROM and describes the ROM/RAM/SRAM bank wiring of the cart.

6 bytes: "EmuSCV"  (0x45, 0x6D, 0x75, 0x53, 0x43, 0x56)
4 bytes: "...."    (0x19, 0x84, 0x07, 0x17) => the SCV was released the 17th july 1984 :-)
4 bytes: "CART"    (0x43, 0x41, 0x52, 0x54)
1 byte : version (= 1)
1 byte : number of ROM/RAM/SRAM blocks (6 max in v1)
2 bytes: descriptor for block 1
2 bytes: descriptor for block 2
2 bytes: descriptor for block 3
2 bytes: descriptor for block 4
2 bytes: descriptor for block 5
2 bytes: descriptor for block 6
4 bytes: CRC32 of the data part
n bytes: data

 .SAVE file format V1
---------------------

The .SAVE format is the native game save format for EmuSCV.
It is relied to a .CART by its CRC32.

6 bytes: "EmuSCV"  (0x45, 0x6D, 0x75, 0x53, 0x43, 0x56)
4 bytes: "...."    (0x19, 0x84, 0x07, 0x17) => the SCV was released the 17th july 1984 :-)
4 bytes: "SAVE"    (0x43, 0x41, 0x52, 0x54)
1 byte : version (= 1)
1 byte : number of SRAM blocks (1 max in v1)
2 bytes: descriptor for block 1
6 bytes: reserved
4 bytes: CRC32 of the data part of the .CART relied file
4 bytes: CRC32 of the data part of the .SAVE file
n bytes: data



To be consinued...
(controls, core options, etc.)
